<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress-genta' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ')DZ?p?uM_1)XwZ6lXwst.HUiUc41/f@zX1zQ*Gho&$90O#aDV@5`N^_2@m|%1bL8' );
define( 'SECURE_AUTH_KEY',  'h [W4/g&^I8v`$9!P{W=6sG@};?rPVf_.4FjRiQV+LBhBH] `GXPDlpdmPNv>JW_' );
define( 'LOGGED_IN_KEY',    'o!)Jv3c~P0EnIElZ~y2IuK1!gv:aT`|uRN;(x}Ws>qS|=0}LDCfJIB_d(Md-3b|B' );
define( 'NONCE_KEY',        'T]RJ+r+/JpeHZU?/X>N&Q&`/Nr(3$#ID)fdW+>| *12oT[:rSo_4QAaEblYT820n' );
define( 'AUTH_SALT',        '-qrZEJ2e DBh=OH[m#kE.cW&hf;3,WVrW6{=at}/K$R8kC3,2-< ^,UkHdp)!~y}' );
define( 'SECURE_AUTH_SALT', 'M?P z}QLoB7}9]R<dtxDJY<loA/!`+bKf[%VeTE8a}f`!;K3CSjt-E$fwA{V{BRG' );
define( 'LOGGED_IN_SALT',   '(I(UAqS4`b1,*Kn[Ps!u!`xy-hlNE]:U-y);+{/jup*.fN;Dc`ZWLBd;/%P=*dh4' );
define( 'NONCE_SALT',       'w]LxAhEu.u3<EUD+{]Rh7>D;&&u4pv},.8SJzG^rGFM;N!]^<4(!qG[Sp#mExj.@' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
